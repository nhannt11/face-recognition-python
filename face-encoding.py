
# python face-encoding.py --dataset dataset --encodings encodings.pickle --detection-method hog

# import library
from imutils import paths
import face_recognition
import argparse
import pickle
import cv2
import os

# Parsing Argumen
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--dataset", required=True,
	help="path to input directory of faces + images")
ap.add_argument("-e", "--encodings", required=True,
	help="path to serialized db of facial encodings")
ap.add_argument("-d", "--detection-method", type=str, default="cnn",
	help="face detection model to use: either `hog` or `cnn`")
args = vars(ap.parse_args())

# lay du lieu tu folder dataset
print("[INFO] dang tai du lieu...")
imagePaths = list(paths.list_images(args["dataset"]))

# nhung khuon mat da biet
knownEncodings = []
knownNames = []

# xu ly anh va ten anh
for (i, imagePath) in enumerate(imagePaths):
	# lay ten cua thu muc anh
	print("[INFO] Dang xu ly anh {}/{}".format(i + 1,
		len(imagePaths)))
	name = imagePath.split(os.path.sep)[-2]

	# Chuyen anh sang RGB
	image = cv2.imread(imagePath)
	rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

	# lay toa do khuon mat
	boxes = face_recognition.face_locations(rgb,
		model=args["detection_method"])

	# Ma hoa khuon mat
	encodings = face_recognition.face_encodings(rgb, boxes)

	# Gan ten va khuon mat da ma hoa
	for encoding in encodings:
		knownEncodings.append(encoding)
		knownNames.append(name)

# luu du lieu da xu ly vao the nho
print("[INFO] Dang xu ly...")
data = {"encodings": knownEncodings, "names": knownNames}
f = open(args["encodings"], "wb")
f.write(pickle.dumps(data))
f.close()
