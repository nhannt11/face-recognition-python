
# python face-recognition-video.py --cascade haarcascade_frontalface_default.xml --encodings encodings.pickle

# import library 
from imutils.video import VideoStream
from imutils.video import FPS
import face_recognition
import argparse
import imutils
import pickle
import time
import cv2

# Parsing Argumen
ap = argparse.ArgumentParser()
ap.add_argument("-c", "--cascade", required=True,
	help = "path to where the face cascade resides")
ap.add_argument("-e", "--encodings", required=True,
	help="path to serialized db of facial encodings")
args = vars(ap.parse_args())

# load file nhan dien haarcascade
print("[INFO] loading file ma hoa cac khuon mat da biet va file nhan dien haarcascade...")
data = pickle.loads(open(args["encodings"], "rb").read())
detector = cv2.CascadeClassifier(args["cascade"])

# khoi dong camera
print("[INFO] bat dau phat  Pi Camera...")
vs = VideoStream(src=0).start()
time.sleep(2.0)

# Lay khung hinh moi giay (FPS)
fps = FPS().start()

# xu ly tung khung hinh
while True:
	# thay doi kich thuoc thanh 500 pixel
	frame = vs.read()
	frame = imutils.resize(frame, width=500)
	
	# chuyen thanh anh xam va anh rgb
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

	# phat hien khuon mat tu anh xam
	rects = detector.detectMultiScale(gray, scaleFactor=1.1, 
		minNeighbors=5, minSize=(30, 30),
		flags=cv2.CASCADE_SCALE_IMAGE)

   # Dong khung khuon mat phat hien
	boxes = [(y, x + w, y + h, x) for (x, y, w, h) in rects]

	encodings = face_recognition.face_encodings(rgb, boxes)
	names = []

	# nhan dien khuon mat da phat hien voi co so du lieu
	for encoding in encodings:
		matches = face_recognition.compare_faces(data["encodings"],
			encoding)
		name = "Unknown"

		#gan ten neu trung voi khuon mat da biet 
		if True in matches:
			matchedIdxs = [i for (i, b) in enumerate(matches) if b]
			counts = {}
			for i in matchedIdxs:
				name = data["names"][i]
				counts[name] = counts.get(name, 0) + 1
			name = max(counts, key=counts.get)
		names.append(name)

	# hien thi ten
	for ((top, right, bottom, left), name) in zip(boxes, names):
		cv2.rectangle(frame, (left, top), (right, bottom),
			(0, 255, 0), 2)
		y = top - 15 if top - 15 > 15 else top + 15
		cv2.putText(frame, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
			0.75, (0, 255, 0), 2)

	# hien thi hinh anh len man hinh
	cv2.imshow("Frame", frame)
	key = cv2.waitKey(1) & 0xFF

	# thoat chuong trinh
	if key == ord("q"):
		break

	# update fps
	fps.update()

# hien thi thong  FPS
fps.stop()
print("[INFO] elasped time: {:.2f}".format(fps.elapsed()))
print("[INFO] approx. FPS: {:.2f}".format(fps.fps()))

# thoat
cv2.destroyAllWindows()
vs.stop()
